# How to contribute to this project

## Workshop participants -- exercise contributions

Contributions to this project should follow these guidelines:

1. Every contribution should solve an issue (whether it is a bug or an enhancement).  
   If an issue describing your requested change does not exist yet, please create
   a new one.

2. When adding or changing text, please keep the style of surrounding text.

3. When adding yourself to the participants list, you also need to add a pet to the
   pets list.

4. In the description of your Merge Request, please include a reference to the issue
   the MR addresses -- e.g. if your code solves issue #3, please include a line
   saying `Closes #3`.

   Also, to make things fun, please attach to the MR an image which, in some
   way, symbolizes the change. It can be a picture of yourself, some random
   image from the internet, or an icon from your computer. But please note: This
   is a public project and MRs are public, so make sure not to violate anybody's
   privacy (yourself included), and to respect copyrights.

5. While your merge request is being reviewed, if you are asked to make changes,
   please make these changes by adding separate commits to your branch, so the
   reviewer may see exactly which changes you've made to address their comments.

   When the suggested changes are satisfactory, you may be asked to squash and
   rebase your commits to make the MR easier to merge.
   
## Everybody -- suggestions to improve the workshop material

1. Every contribution should solve an issue (whether it is a bug or an enhancement).  
   If an issue describing your requested change does not exist yet, please create
   a new one.

2. When adding or changing text, please keep the style of surrounding text.

(yes, these are the same as the first two above)

Thanks!